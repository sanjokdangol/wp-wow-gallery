<?php
/**
 * Register Gallery Scripts
 */
if(!class_exists('Wow_Gallery_Functions')){
    class Wow_Gallery_Functions {

        /**
         * Debugging function
         * Used in development mode
         *
         * @param [type] $data
         * @return void
         */
        function dd($data) {
            if(WP_DEBUG && $_SERVER['SERVER_NAME']=="localhost") {
                highlight_string("<?php\n\$output =\n" . var_export($data, true) . ";\n?>");
                die();
            }            
        }

        /**
         * Fetch all album images by album id
         *
         * @param [type] $post_id
         * @return void
         */
        function get_by_post($post_id) {
            $images = array();
            $ids = get_post_meta( $post_id, 'wow_gallery_id', true );
            if($ids) {
                foreach ( $ids as $key => $value ) :
                    $image = wp_get_attachment_image_src( $value );
                    array_push($images, $image[0]);
                endforeach;                
            }
            return $images;
        }

        /**
         * Fetch all gallery
         *
         * @param string $fields
         * @return void
         */
        function get_all($fields = "*") {
            global $wpdb;
            return $wpdb->get_results("SELECT ". $fields ." FROM $wpdb->posts WHERE post_type='gallery'");
        }

        /**
         * Fetch album cover image url
         *
         * @param [type] $post_id
         * @param string $size
         * @return void
         */
        function get_cover_image_by_post_id($post_id, $size = "post-thumbnail") {
            $post_thumbnail_id = get_post_thumbnail_id( $post_id );
            return wp_get_attachment_image_url( $post_thumbnail_id, $size );
        }
    }       
    
}

$fun = new Wow_Gallery_Functions();
// $fun->get_by_post(158);
//$fun->dd($_SERVER['SERVER_NAME']);
