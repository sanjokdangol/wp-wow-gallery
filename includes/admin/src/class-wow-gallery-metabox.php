<?php
class Wow_Gallery_Meta_Box {
		
	protected $post_type;

	function __construct() {
			$this->post_type = "gallery";
			$this->wow_add_actions();
	}

	public function wow_add_actions() {       
			add_action( 'add_meta_boxes', array( $this, 'wow_add_gallery_meta_box' ), 0 );
			add_action( 'save_post', array( $this, 'wow_gallery_meta_save' ), 0 );
	}

	function wow_add_gallery_meta_box() {
			add_meta_box("feat-img-gallery-metabox", "Photo Gallery", array( $this, "wow_gallery_meta_fields"), $this->post_type, 'normal', 'high' );
	}

  function wow_gallery_meta_fields($post) {
        wp_nonce_field( basename( __FILE__ ), 'wow_gallery_meta_nonce' );
				$ids = get_post_meta( $post->ID, 'wow_gallery_id', true );
		?>
				<table class='form-table'>
					<tr><td>
					<label for='wow_gallery_id[enable]'><?php _e( 'Enable Gallery', "wow-gallery" ); ?></label>
					<input type='checkbox' name='wow_gallery_id[enable]' id='wow_gallery_id[enable]' <?php $j = isset( $ids['enable'] ) ? esc_attr( $ids['enable'] ) : '0'; ?> value='1' <?php checked( $j, true ); ?>/>
					<div class="settings-note"><?php _e( 'Check this to enable gallery instead of featured image in single trip.', "wow-gallery" ); ?></div>
					</td></tr>
					<tr><td>
					<a class='feat-img-gallery-add button' href='#' data-uploader-title='Add image(s) to gallery' data-uploader-button-text='Add image(s)'><?php _e( 'Add image(s)', 'wow-gallery' ); ?></a>
					<ul id='feat-img-gallery-metabox-list'>
					<?php
					if ( $ids ) :
						unset( $ids['enable'] );
						foreach ( $ids as $key => $value ) :
							$image = wp_get_attachment_image_src( $value );
							?>
						<li>
						<input type='hidden' name='wow_gallery_id[<?php echo $key; ?>]' value='<?php echo $value; ?>'>
						<img class='image-preview' src='<?php echo $image[0]; ?>'>
						<a class='change-image button button-small' href='#' data-uploader-title='Change image' data-uploader-button-text='Change image'>Change image</a><br>
						<small><a class='remove-image' href='#'><?php _e( 'Remove image', "wow-gallery" ); ?></a></small>
						</li>
							<?php
					endforeach;
						endif;
					?>
					</ul>
					</td></tr>
				</table>
		<?php
    }
    
    function wow_gallery_meta_save( $post_id ) {
		if ( ! isset( $_POST['wow_gallery_meta_nonce'] ) || ! wp_verify_nonce( $_POST['wow_gallery_meta_nonce'], basename( __FILE__ ) ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( isset( $_POST['wow_gallery_id'] ) ) {
			update_post_meta( $post_id, 'wow_gallery_id', $_POST['wow_gallery_id'] );
		} else {
			delete_post_meta( $post_id, 'wow_gallery_id' );
		}
	}
}
if(class_exists('Wow_Gallery_Meta_Box')) {
    $wow_meta_box = new Wow_Gallery_Meta_Box();
}