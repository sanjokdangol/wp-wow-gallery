<?php
class Wow_Gallery_Query{

    protected $post_type;

    function __construct() {
        $this->post_type = "gallery";
    }

    function get_all(){
        global $wpdb;
        $sql = "SELECT * FROM ". $wpdb->posts . " WHERE post_type ='" . $this->post_type . "'" ;
        return $wpdb->get_results($sql);
    }
    function get_album_by_id($id) {
        global $wpdb;
        return $wpdb->get_row( "SELECT * FROM " . $wpdb->posts . "WHERE post_type='".$this->post_type."' AND ID=" . $id );
    }

    function insert($array) {
        global $wpdb;
        $wpdb->insert( $wpdb->posts, $array);
    }

    function delete_post_meta($post_id) {
        global $wpdb;
        return $wpdb->query(
            $wpdb->prepare( "DELETE FROM $wpdb->posts WHERE post_id = %d", $post_id));
    }

    function delete($post_id) {
        global $wpdb;
        return $wpdb->query(
            $wpdb->prepare( "DELETE FROM $wpdb->posts WHERE ID = %d", $post_id));
    }

    function del(){
        global $wpdb;
        return $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type='gallerys'");
    }
}

$query = new Wow_Gallery_Query();

//var_dump($query->get_album_by_id(97));