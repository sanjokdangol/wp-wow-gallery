<?php
/*
Plugin Name: Wow Gallery
Plugin URI: 
Description: gallery
Author: sanjok dangol
Author URI: https://sanjokdangol.com.np/
Version: 0.6.4
Text Domain: wow-gallery
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/
if(!defined('ABSPATH'))
    exit;
    
if(file_exists( __DIR__ . '/wow-gallery-includes.php' )) {
    require_once( __DIR__ . '/wow-gallery-includes.php' );
}

if(!class_exists('Wow_Gallery')) {
    class Wow_Gallery {

        function __construct()
        {
            $this->wow_gallery_add_actions();
        }

        function wow_gallery_add_actions() {
            add_action('admin_head', 'register_wow_gallery_styles');
            add_action('admin_enqueue_scripts', 'register_wow_gallery_scripts');
            add_action('init', array($this, 'register_custom_post_type'));
           
        }

       

        function register_custom_post_type() {
            $labels = array(
                'name'               => _x( 'Album', 'post type general name', 'wow-Gallery' ),
                'singular_name'      => _x( 'Gallery', 'post type singular name', 'wow-Gallery' ),
                'menu_name'          => _x( 'Photo Gallery', 'admin menu', 'wow-Gallerys' ),
                'name_admin_bar'     => _x( 'Gallery', 'add new on admin bar', 'wow-Gallery' ),
                'add_new'            => _x( 'Add New', 'Galleries', 'wow-Gallery' ),
                'add_new_item'       => __( 'Add New Album', 'wow-Gallery' ),
                'new_item'           => __( 'New Album', 'wow-Gallery' ),
                'edit_item'          => __( 'Edit Album', 'wow-Gallery' ),
                'view_item'          => __( 'View Album', 'wow-Gallerys' ),
                'all_items'          => __( 'All Albums', 'wow-Gallery' ),
                'search_items'       => __( 'Search Album', 'wow-Gallery' ),
                'parent_item_colon'  => __( 'Parent Gallery:', 'wow-Gallery' ),
                'not_found'          => __( 'No Gallery found.', 'wow-Gallery' ),
                'not_found_in_trash' => __( 'No Gallery found in Trash.', 'wow-Gallery' ),
                'featured_image' => __( 'Album Cover Photo' ),
				'set_featured_image' => __( 'Set Album Cover Photo' ),
				'remove_featured_image' => __( 'Remove Album Cover Photo' ),
				'use_featured_image' => __( 'Use as Album Cover Photo' )
             );
       
             $args = array(
                'labels'             => $labels,
                'description'        => __( 'Description.', 'wow-Gallerys' ),
                'public'             => true,
                'menu_icon'          => 'dashicons-format-gallery',
                'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'featured image' ),
         
             );
       
             register_post_type( 'gallery', $args );
        }
        function activate() {
            $this->wow_gallery_add_actions();
            flush_rewrite_rules();
        }

        function deactivate() {
            flush_rewrite_rules();
        }

        
        /**
         * Register Gallery Styles
         */
           
        function register_wow_gallery_styles() {
            
            wp_enqueue_style('wow-gallery-metabox', plugins_url('/includes/admin/css/gallery-metabox.css',  __FILE__ ) );
            wp_enqueue_style('wow-gallery-admin', plugins_url('/includes/admin/css/gallery-admin.css',  __FILE__ ) );
            
        }
       
        /**
         * Register Gallery Scripts
         */       
        function register_wow_gallery_scripts() {
            
                wp_enqueue_script('wow-gallery', plugins_url('/includes/admin/js/gallery-metabox.js',  __FILE__ ), array(), '', true);
                wp_enqueue_script('wow-gallerymedia-upload', plugins_url('/includes/admin/js/media-upload.js',  __FILE__ ), array(), '', true);
                
        }
        
    }
}

if(class_exists('Wow_Gallery')) {
    $wow_gallery = new Wow_Gallery();
    register_activation_hook(__FILE__, array($wow_gallery, 'activate'));
    register_deactivation_hook(__FILE__, array( $wow_gallery, 'deactivate'));
  
  }