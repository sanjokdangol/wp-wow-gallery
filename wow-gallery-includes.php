<?php
/**
 * Define constants
 */
if (!defined('WOW_GALLERY_DIR'))
    define('WOW_GALLERY_DIR', dirname(__FILE__) .'/');

if (!defined('WOW_GALLERY_ADMIN'))
    define('WOW_GALLERY_ADMIN', WOW_GALLERY_DIR . '/includes/admin/src/');

if (!defined('MYPLUGIN_THEME_DIR'))
    define('MYPLUGIN_THEME_DIR', ABSPATH . 'wp-content/themes/' . get_template());

if (!defined('WOW_GALLERY_PLUGIN_NAME'))
    define('WOW_GALLERY_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));

if (!defined('WOW_GALLERY_PLUGIN_URL'))
    define('WOW_GALLERY_PLUGIN_URL', WOW_GALLERY_DIR . '/' . WOW_GALLERY_PLUGIN_NAME);

if (!defined('WOW_GALLERY_VERSION_KEY'))
    define('WOW_GALLERY_VERSION_KEY', 'myplugin_version');

if (!defined('WOW_GALLERY_VERSION_NUM'))
    define('WOW_GALLERY_VERSION_NUM', '1.0.0');

add_option(MYPLUGIN_VERSION_KEY, MYPLUGIN_VERSION_NUM);

$new_version = '2.0.0';

if (get_option(MYPLUGIN_VERSION_KEY) != $new_version) {
    // Execute your upgrade logic here

    // Then update the version value
    update_option(MYPLUGIN_VERSION_KEY, $new_version);
}

/**
 * All Admin common functions
 */
if(file_exists( WOW_GALLERY_DIR . 'wow-gallery-functions.php' )) {
    require_once( WOW_GALLERY_DIR . 'wow-gallery-functions.php' );
}

/**
 * Register metabox
 */
if(file_exists( WOW_GALLERY_ADMIN . 'class-wow-gallery-metabox.php' )) {
    require_once( WOW_GALLERY_ADMIN . 'class-wow-gallery-metabox.php' );
}

/**
 * Database Query functions
 */
if(file_exists( WOW_GALLERY_ADMIN . 'class-wow-gallery-query.php' )) {
    require_once( WOW_GALLERY_ADMIN . 'class-wow-gallery-query.php' );
}